function message(content,element)
{
    var div = $('<div>').attr('class','alert alert-warning').attr('role','alert');
    var icon = $('<i>').attr('class','fa fa-warning');
    var h6 = $('<h6>').attr('class','title').text(content['title']);
    var p = $('<p>').text(content['message']);

    $(div).append($(icon)).append($(h6)).append($(p));
    $(element).append($(div));
}