<?php
/**
 * Created by PhpStorm.
 * User: Mehmet HAKKIOĞLU
 * Mail: contact@mehmethakkioglu.com
 * Date: 03.12.2018
 * Time: 15:59
 */
namespace Feedback\Repository;

use Core\Classes\MethaDatabase;
use Zend\Db\TableGateway\TableGateway;

class FeedbackCommentRespository
{
    private $adapter;

    public function __construct()
    {
        $adapter = new MethaDatabase();
        $this->adapter = $adapter->MethaDatabaseConnection();
    }

    public function feedbackCommentSave(array $data)
    {
        $table = new TableGateway('feedback_comments',$this->adapter);
        return $table->insert($data);
    }

    public function feedbackCommentList()
    {
        $dataList = [];
        $table = new TableGateway('feedback_comments',$this->adapter);
        $rows = $table->select();

        foreach ($rows as $item)
        {
            $dataList[$item['feedback_id']][] = [
                'id' => $item['id'],
                'feedback_id' => $item['feedback_id'],
                'user_id' => $item['user_id'],
                'comment' => $item['comment'],
                'create_date' => $item['create_date']
            ];
        }

        return $dataList;
    }

    public function feedbackCommentFindByOneList(string $query)
    {
        $dataList = [];
        $table = new TableGateway('feedback_comments',$this->adapter);
        $rows = $table->select($query);

        foreach ($rows as $item)
        {
            $dataList = [
                'id' => $item['id'],
                'feedback_id' => $item['feedback_id'],
                'user_id' => $item['user_id'],
                'comment' => $item['comment'],
                'create_date' => $item['create_date']
            ];
        }

        return $dataList;
    }
}