<?php
/**
 * Created by PhpStorm.
 * User: Mehmet HAKKIOĞLU
 * Mail: contact@mehmethakkioglu.com
 * Date: 03.12.2018
 * Time: 04:38
 */
namespace Feedback\Repository;

use Core\Classes\MethaDatabase;
use Zend\Db\TableGateway\TableGateway;

class FeedbackRepository
{
    private $adapter;

    public function __construct()
    {
        $adapter = new MethaDatabase();
        $this->adapter = $adapter->MethaDatabaseConnection();
    }

    public function feedbackSave(array $data)
    {
        $table = new TableGateway('feedback_core',$this->adapter);
        return $table->insert($data);
    }

    public function feedbackList()
    {
        $dataList = [];
        $table = new TableGateway('feedback_core',$this->adapter);
        $rows = $table->select();

        foreach ($rows as $item)
        {
            $dataList[] = [
                'id' => $item['id'],
                'user_id' => $item['user_id'],
                'media' => $item['media'],
                'type' => $item['type'],
                'create_date' => $item['create_date'],
                'rating' => $item['rating'],
            ];
        }

        return $dataList;
    }

    public function feedbackFindByOne(string $query)
    {
        $dataList = [];
        $table = new TableGateway('feedback_core',$this->adapter);
        $rows = $table->select($query);

        foreach ($rows as $item)
        {
            $dataList = [
                'id' => $item['id'],
                'user_id' => $item['user_id'],
                'media' => $item['media'],
                'type' => $item['type'],
                'create_date' => $item['create_date'],
                'rating' => $item['rating'],
            ];
        }

        return $dataList;
    }
}