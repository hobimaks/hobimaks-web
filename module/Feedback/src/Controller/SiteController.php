<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Feedback\Controller;

use Core\Classes\MethaDatabase;
use Feedback\Repository\FeedbackCommentRespository;
use Feedback\Repository\FeedbackRepository;
use User\Repository\UserRepository;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class SiteController extends AbstractActionController
{
    private $feedbackRepository;
    private $feedbackCommentRepository;
    private $userInfoRepository;
    private $methaDatabase;

    public function __construct()
    {
        $this->feedbackRepository = new FeedbackRepository();
        $this->feedbackCommentRepository = new FeedbackCommentRespository();
        $this->userInfoRepository = new UserRepository();
        $this->methaDatabase = new MethaDatabase();
    }

    public function feedbackAction()
    {
        $view = new ViewModel([
            'feedbackList' => $this->feedbackRepository->feedbackList(),
            'feedbackCommentList' => $this->feedbackCommentRepository->feedbackCommentList(),
            'userList' => $this->userInfoRepository->userInfoList()
        ]);
        $view->setTemplate('page/site/feedback-index');
        $this->layout()->setTemplate('layout/feedback_layout')->setVariables([
            'nav' => 'home',
            'subTitle' => 'Geri Dönüsler'
        ]);
        return $view;
    }

    public function viewAction()
    {
        $id = $this->params()->fromRoute('id');
        $view = new ViewModel([
            'id' => $id,
            'feedbackList' => $this->feedbackRepository->feedbackFindByOne(sprintf('id=%d',$id)),
            'feedbackCommentList' => $this->feedbackCommentRepository->feedbackCommentList(),
            'userInfo' => $this->userInfoRepository->userInfoList()
        ]);

        if($this->getRequest()->isPost())
        {
            $formData = $this->params()->fromPost();
            $this->feedbackCommentRepository->feedbackCommentSave([
                'feedback_id' => $id,
                'user_id' => $_SESSION['userInfo']['id'],
                'comment' => $formData['comment'],
                'create_date' => date('Y-m-d H:i:s')
            ]);

            return $this->redirect()->toRoute('site/feedback/view',['id' => $id]);
        }

        $view->setTemplate('page/site/feedback-view');
        $this->layout()->setTemplate('layout/feedback_layout')->setVariables([
            'nav' => 'home',
            'subTitle' => 'Geri Dönüsler',
            'type' => 'view'
        ]);
        return $view;
    }

    public function createAction()
    {
        $view = new ViewModel();
        $view->setTemplate('page/site/feedback-create');
        $this->layout()->setTemplate('layout/user_site_layout')->setVariables([
            'subTitle' => 'Feedback',
            'user' => 'feedback',
            'userPanel' => 'feedback'
        ]);

        if($this->getRequest()->isPost())
        {
            $formData = $this->params()->fromPost();

            if($formData['type'] == 'text')
            {
                $result = $this->feedbackRepository->feedbackSave([
                    'user_id' => $_SESSION['userInfo']['id'],
                    'media' => json_encode($formData['media']),
                    'type' => $formData['type'],
                    'create_date' => date('Y-m-d H:i:s')
                ]);

                if($result)
                {
                    return $this->redirect()->toRoute('site/user/feedback');
                }else{
                    $_SESSION['message'] = [
                        'code' => 201,
                        'text' => 'Beklenmedik bir hata oluştu.Lütfen daha sonra tekrar deneyiniz...'
                    ];
                }
            }else{
                $media = $this->params()->fromFiles();
                $upload = $this->methaDatabase->imageUpload($media);
                if($upload)
                {
                    $result = $this->feedbackRepository->feedbackSave([
                        'user_id' => $_SESSION['userInfo']['id'],
                        'media' => json_encode(['text' => $formData['media']['text'], 'content' => $upload]),
                        'type' => $formData['type'],
                        'create_date' => date('Y-m-d H:i:s')
                    ]);

                    if($result)
                    {
                        return $this->redirect()->toRoute('site/user/feedback');
                    }else{
                        $_SESSION['message'] = [
                            'code' => 201,
                            'text' => 'Beklenmedik bir hata oluştu.Lütfen daha sonra tekrar deneyiniz...'
                        ];
                    }
                }
            }
        }

        if(isset($_SESSION['hobimaksSession']) == true)
        {
            return $view;
        }else{
            return $this->redirect()->toRoute('site/user');
        }
    }
}
