<?php
/**
 * Created by PhpStorm.
 * User: Mehmet HAKKIOĞLU
 * Mail: contact@mehmethakkioglu.com
 * Date: 16.11.2018
 * Time: 02:59
 */
namespace Core\Classes;
use Zend\Db\Adapter\Adapter;

class MethaDatabase
{
    public function MethaDatabaseConnection()
    {
        $adapter = new Adapter([
            'driver'   => 'Mysqli',
            'database' => 'hobimaks',
            'username' => 'root',
            'password' => 'pct1616',
            'charset'  => 'utf8',
        ]);

        return $adapter;
    }

    function methaSlug($text) {
        $text = trim($text);
        $search = array('Ç','ç','Ğ','ğ','ı','İ','Ö','ö','Ş','ş','Ü','ü',' ','I');
        $replace = array('c','c','g','g','i','i','o','o','s','s','u','u','-','i');
        $new_text = str_replace($search,$replace,$text);
        return strtolower($new_text);
    }

    public function imageUpload(array $data)
    {
        $dizin = '/var/www/html/hobimaks-web/public/feedbacks/';
        $name = null;

        switch ($data['media']['content']['type']) {
            case 'image/jpeg':
                $name = hash('sha256',md5(substr($data['media']['content']['name'],0,-3).date('Y-m-d H:i:s'))).'.jpg';
        break;
            case 'image/png':
                $name = hash('sha256',md5(substr($data['media']['content']['name'],0,-3).date('Y-m-d H:i:s'))).'.png';
        break;
            case 'image/gif':
                $name = hash('sha256',md5(substr($data['media']['content']['name'],0,-3).date('Y-m-d H:i:s'))).'.gif';
        }

        $yuklenecek_dosya = $dizin . basename($name);

        if (move_uploaded_file($data['media']['content']['tmp_name'], $yuklenecek_dosya))
        {
            return basename($name);
        } else {
            return false;
        }
    }
}