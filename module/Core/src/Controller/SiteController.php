<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Core\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class SiteController extends AbstractActionController
{
    public function indexAction()
    {
        $view = new ViewModel();
        $view->setTemplate('page/site/index');
        $this->layout()->setTemplate('layout/layout')->setVariables([
            'nav' => 'home'
        ]);
        return $view;
    }

    public function aboutAction()
    {
        $view = new ViewModel();
        $view->setTemplate('page/site/about');
        $this->layout()->setTemplate('layout/layout')->setVariables([
            'subTitle' => 'Hakkımızda',
            'nav' => 'about'
        ]);
        return $view;
    }
}
