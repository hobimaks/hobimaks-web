<?php
/**
 * Created by PhpStorm.
 * User: Mehmet HAKKIOĞLU
 * Mail: contact@mehmethakkioglu.com
 * Date: 02.12.2018
 * Time: 16:20
 */
namespace Product\Repository;

use Core\Classes\MethaDatabase;
use Zend\Db\TableGateway\TableGateway;

class ProductRepository
{
    private $adapter;

    public function __construct()
    {
        $adapter = new MethaDatabase();
        $this->adapter = $adapter->MethaDatabaseConnection();
    }

    public function productave(array $data)
    {
        $table = new TableGateway('product',$this->adapter);
        return $table->insert($data);
    }

    public function productUpdate(array $data)
    {
        $table = new TableGateway('product',$this->adapter);
        return $table->update($data['datas'],['id' => $data['id']]);
    }

    public function productRemove(int $id)
    {
        $table = new TableGateway('product',$this->adapter);
        return $table->delete(['id' => $id]);
    }

    public function productList()
    {
        $dataList = [];
        $table = new TableGateway('product',$this->adapter);
        $rows = $table->select();

        foreach ($rows as $item)
        {
            $dataList[] = [
                'id' => $item['id'],
                'title' => $item['title'],
                'slug' => $item['slug'],
                'description' => $item['description'],
                'create_date' => $item['create_date'],
                'qr_code' => $item['qr_code'],
                'price' => $item['price'],
                'status' => $item['status']
            ];
        }

        return $dataList;
    }

    public function productFindByOneList(string $query)
    {
        $dataList = [];
        $table = new TableGateway('product',$this->adapter);
        $rows = $table->select($query);

        foreach ($rows as $item)
        {
            $dataList = [
                'id' => $item['id'],
                'title' => $item['title'],
                'slug' => $item['slug'],
                'description' => $item['description'],
                'create_date' => $item['create_date'],
                'qr_code' => $item['qr_code'],
                'price' => $item['price'],
                'status' => $item['status']
            ];
        }

        return $dataList;
    }

    public function addBag(array $data)
    {
        $table = new TableGateway('shop_bag',$this->adapter);
        return $table->insert($data);
    }

    public function removeBag(int $id)
    {
        $table = new TableGateway('shop_bag',$this->adapter);
        return $table->delete(['id' => $id]);
    }

    public function removeAllBag(int $id)
    {
        $table = new TableGateway('shop_bag',$this->adapter);
        return $table->delete(['user_id' => $id]);
    }

    public function bagList(int $id)
    {
        $dataList = [];
        $table = new TableGateway('shop_bag',$this->adapter);
        $rows = $table->select(sprintf('user_id=%d',$id),$this->adapter);

        foreach ($rows as $item)
        {
            $dataList[] = [
                'id' => $item['id'],
                'product_id' => $item['product_id'],
                'quantity' => $item['quantity'],
                'create_date' => $item['create_date']
            ];
        }

        return $dataList;
    }

    public function bagFindByOneList(array $data)
    {
        $dataList = [];
        $table = new TableGateway('shop_bag',$this->adapter);
        $rows = $table->select(sprintf('user_id=%d AND product_id=%d',$data['user_id'],$data['product_id']),$this->adapter);

        foreach ($rows as $item)
        {
            $dataList[] = [
                'id' => $item['id'],
                'product_id' => $item['product_id'],
                'quantity' => $item['quantity'],
                'create_date' => $item['create_date']
            ];
        }

        return $dataList;
    }

    public function bagUpate(array $data)
    {
        $table = new TableGateway('shop_bag',$this->adapter);
        return $table->update($data['datas'],['id' => $data['id']]);
    }
}