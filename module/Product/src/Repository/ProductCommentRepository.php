<?php
/**
 * Created by PhpStorm.
 * User: Mehmet HAKKIOĞLU
 * Mail: contact@mehmethakkioglu.com
 * Date: 03.12.2018
 * Time: 03:25
 */
namespace Product\Repository;

use Core\Classes\MethaDatabase;
use Zend\Db\TableGateway\TableGateway;

class ProductCommentRepository
{
    private $adapter;

    public function __construct()
    {
        $adapter = new MethaDatabase();
        $this->adapter = $adapter->MethaDatabaseConnection();
    }

    public function commentSave(array $data)
    {
        $table = new TableGateway('product_comments',$this->adapter);
        return $table->insert($data);
    }

    public function commentList()
    {
        $dataList = [];
        $table = new TableGateway('product_comments',$this->adapter);
        $rows = $table->select();

        foreach ($rows as $item)
        {
            $dataList[$item['product_id']][] = [
                'id' => $item['id'],
                'product_id' => $item['product_id'],
                'user_id' => $item['user_id'],
                'comment' => $item['comment'],
                'create_date' => $item['create_date'],
                'status' => $item['status']
            ];
        }

        return $dataList;
    }
}