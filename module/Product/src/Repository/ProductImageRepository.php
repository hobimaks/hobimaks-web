<?php
/**
 * Created by PhpStorm.
 * User: Mehmet HAKKIOĞLU
 * Mail: contact@mehmethakkioglu.com
 * Date: 02.12.2018
 * Time: 16:20
 */
namespace Product\Repository;

use Core\Classes\MethaDatabase;
use Zend\Db\TableGateway\TableGateway;

class ProductImageRepository
{
    private $adapter;

    public function __construct()
    {
        $adapter = new MethaDatabase();
        $this->adapter = $adapter->MethaDatabaseConnection();
    }

    public function productImageSave(array $data)
    {
        $table = new TableGateway('product_to_image',$this->adapter);
        return $table->insert($data);
    }

    public function productImageUpdate(array $data)
    {
        $table = new TableGateway('product',$this->adapter);
        return $table->update($data['datas'],['id' => $data['id']]);
    }

    public function productImageRemove(int $id)
    {
        $table = new TableGateway('product_to_image',$this->adapter);
        return $table->delete(['id' => $id]);
    }

    public function productImageList()
    {
        $dataList = [];
        $table = new TableGateway('product_to_image',$this->adapter);
        $rows = $table->select();

        foreach ($rows as $item)
        {
            $dataList[$item['product_id']][] = [
                'id' => $item['id'],
                'product_id' => $item['product_id'],
                'image' => $item['image']
            ];
        }

        return $dataList;
    }

    public function productImageFindByOneList(string $query)
    {
        $dataList = [];
        $table = new TableGateway('product_to_image',$this->adapter);
        $rows = $table->select($query);

        foreach ($rows as $item)
        {
            $dataList[$item['product_id']][] = [
                'id' => $item['id'],
                'product_id' => $item['product_id'],
                'image' => $item['image']
            ];
        }

        return $dataList;
    }
}