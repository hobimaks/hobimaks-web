<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Product\Controller;

use Order\Repository\OrderRepository;
use Product\Repository\ProductCommentRepository;
use Product\Repository\ProductRepository;
use Product\Repository\ProductImageRepository;
use User\Repository\UserRepository;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class SiteController extends AbstractActionController
{
    private $productRepository;
    private $productImageRepository;
    private $orderRepository;
    private $productCommentRepository;
    private $userRepository;

    public function __construct()
    {
        $this->productRepository = new ProductRepository();
        $this->productImageRepository = new ProductImageRepository();
        $this->orderRepository = new OrderRepository();
        $this->productCommentRepository = new ProductCommentRepository();
        $this->userRepository = new UserRepository();
    }

    public function productAction()
    {
        $view = new ViewModel([
            'productList' => $this->productRepository->productList(),
            'productImageList' => $this->productImageRepository->productImageList()
        ]);
        $view->setTemplate('page/site/product-index');
        $this->layout()->setTemplate('layout/product_layout')->setVariables([
            'subTitle' => 'Ürün <span>Listele</span>',
            'nav' => 'product'
        ]);
        return $view;
    }

    public function viewAction()
    {
        $slug = $this->params()->fromRoute('slug');
        $productInfo = $this->productRepository->productFindByOneList(sprintf('slug="%s"',$slug));

        $view = new ViewModel([
            'product' => $productInfo,
            'images' => $this->productImageRepository->productImageFindByOneList(sprintf('product_id=%d',$productInfo['id'])),
            'comments' => $this->productCommentRepository->commentList()[$productInfo['id']],
            'userList' => $this->userRepository->userInfoList()
        ]);
        $view->setTemplate('page/site/product-view');
        $this->layout()->setTemplate('layout/product_layout')->setVariables([
            'subTitle' => 'Ürün <span>Incele</span>',
            'nav' => 'product'
        ]);
        return $view;
    }

    public function addBagAction()
    {
        header('Content-Type: application/json');

        $formData = $this->params()->fromPost();

        if(count($this->productRepository->bagFindByOneList([
            'user_id' => $_SESSION['userInfo']['id'],
            'product_id' => $formData['id']
        ])) == 0)
        {
            if($formData['type'] == 'user')
            {
                $result = $this->productRepository->addBag([
                    'user_id' => $_SESSION['userInfo']['id'],
                    'product_id' => $formData['id'],
                    'quantity' => 1,
                    'create_date' => date('Y-m-d H:i:s')
                ]);

                if($result)
                {
                    print_r(json_encode([
                        'code' => 200
                    ]));
                }else{
                    print_r(json_encode([
                        'code' => 201
                    ]));
                }
            }elseif($formData['type'] == 'agent_web')
            {
                $result = $this->orderRepository->agentSave([
                    'product_id' => $formData['id'],
                    'name_surname' => $formData['name_surname'],
                    'gsm' => $formData['gsm'],
                    'create_date' => date('Y-m-d H:i:s')
                ]);

                if($result)
                {
                    print_r(json_encode([
                        'code' => 200
                    ]));
                }else{
                    print_r(json_encode([
                        'code' => 201
                    ]));
                }
            }
        }
        die;
    }

    public function addCommentAction()
    {
        header('Content-Type: application/json');

        $formData = $this->params()->fromPost();
        $formData = array_merge($formData,[
            'user_id' => $_SESSION['userInfo']['id'],
            'create_date' => date('Y-m-d H:i:s'),
            'status' => 'N'
        ]);

        $result = $this->productCommentRepository->commentSave($formData);
        if($result)
        {
            print_r(json_encode(['code' => 200]));
        }else{
            print_r(json_encode(['code' => 201]));
        }
        die;
    }
}
