<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Order;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'router' => [
        'routes' => [
            'site' => [
                'child_routes' => [
                    'order' => [
                        'type' => Segment::class,
                        'may_terminate' => true,
                        'options' => [
                            'route'    => 'order[/]',
                            'defaults' => [
                                'controller' => Controller\SiteController::class,
                                'action'     => 'order',
                            ],
                        ],
                        'child_routes' => [
                            'view' => [
                                'type' => Segment::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => 'view/:slug[/]',
                                    'defaults' => [
                                        'controller' => Controller\SiteController::class,
                                        'action'     => 'view',
                                    ],
                                ],
                            ],
                            'remove' => [
                                'type' => Segment::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => 'remove/:id[/]',
                                    'defaults' => [
                                        'controller' => Controller\SiteController::class,
                                        'action'     => 'remove',
                                    ],
                                ],
                            ],
                            'removeAll' => [
                                'type' => Segment::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => 'removeAll[/]',
                                    'defaults' => [
                                        'controller' => Controller\SiteController::class,
                                        'action'     => 'removeAll',
                                    ],
                                ],
                            ],
                            'payment' => [
                                'type' => Segment::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => 'payment[/]',
                                    'defaults' => [
                                        'controller' => Controller\SiteController::class,
                                        'action'     => 'payment',
                                    ],
                                ],
                            ],
                        ]
                    ],
                ]
            ],
            'panel' => [
                'child_routes' => [
                    'order' => [
                        'type'    => Segment::class,
                        'options' => [
                            'route'    => 'order[/]',
                            'defaults' => [
                                'controller' => Controller\PanelController::class,
                                'action'     => 'index',
                            ],
                        ],
                    ],
                ]
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\PanelController::class => InvokableFactory::class,
            Controller\SiteController::class => InvokableFactory::class,
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
];
