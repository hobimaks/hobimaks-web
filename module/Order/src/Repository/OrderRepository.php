<?php
/**
 * Created by PhpStorm.
 * User: Mehmet HAKKIOĞLU
 * Mail: contact@mehmethakkioglu.com
 * Date: 16.11.2018
 * Time: 06:50
 */
namespace Order\Repository;

use Core\Classes\MethaDatabase;
use Zend\Db\TableGateway\TableGateway;

class OrderRepository
{
    private $adapter;

    public function __construct()
    {
        $adapter = new MethaDatabase();
        $this->adapter = $adapter->MethaDatabaseConnection();
    }

    public function orderSave(array $data)
    {
        $table = new TableGateway('order_core',$this->adapter);
        return $table->insert([
            'user_id' => $data['user_id'],
            'product_id' => $data['product_id'],
            'price' => $data['price'],
            'status' => $data['status'],
            'quantity' => $data['quantity'],
            'order_user' => $data['order_user'],
            'address' => $data['address'],
            'create_date' => date('Y-m-d H:i:s')
        ]);
    }

    public function orderList(int $id)
    {
        $dataList = [];
        $table = new TableGateway('order_core',$this->adapter);
        $rows = $table->select(sprintf('user_id=%d',$id));

        foreach ($rows as $item)
        {
            $dataList[] = [
                'id' => $item['id'],
                'product_id' => $item['product_id'],
                'price' => $item['price'],
                'status' => $item['status'],
                'quantity' => $item['quantity'],
                'create_order' => $item['create_order'],
                'address' => $item['address'],
                'order_user' => $item['order_user'],
            ];
        }

        return $dataList;
    }

    public function agentSave(array $data)
    {
        $table = new TableGateway('agent_order',$this->adapter);
        return $table->insert($data);
    }

    public function pendingOrders(array $data)
    {
        $table = new TableGateway('pending_orders',$this->adapter);
        return $table->insert($data);
    }

    public function pendingOrdersList(int $id)
    {
        $dataList = [];
        $table = new TableGateway('pending_orders',$this->adapter);
        $rows = $table->select(sprintf('user_id=%d',$id));

        foreach ($rows as $item)
        {
            $dataList[] = [
                'id' => $item['id'],
                'user_id' => $item['user_id'],
                'products' => $item['products'],
                'address' => $item['address'],
                'create_date' => $item['create_date'],
                'price' => $item['price'],
                'options' => $item['options'],
                'order_code' => $item['order_code'],
                'status' => $item['status']
            ];
        }

        return $dataList;
    }
}