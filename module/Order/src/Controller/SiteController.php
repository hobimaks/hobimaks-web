<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Order\Controller;

use Order\Repository\OrderRepository;
use Product\Repository\ProductRepository;
use Product\Repository\ProductImageRepository;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class SiteController extends AbstractActionController
{
    private $orderRepository;
    private $productRepository;
    private $productImageRepository;

    public function __construct()
    {
        $this->orderRepository = new OrderRepository();
        $this->productRepository = new ProductRepository();
        $this->productImageRepository = new ProductImageRepository();
    }

    public function orderAction()
    {
        $dataList = [];

        foreach ($this->productRepository->bagList($_SESSION['userInfo']['id']) as $item)
        {
            $product = $this->productRepository->productFindByOneList(sprintf('id=%d',$item['product_id']));
            $dataList[] = [
                'id' => $item['id'],
                'title' => $product['title'],
                'image' => $this->productImageRepository->productImageFindByOneList(sprintf('product_id=%d',$item['product_id']))[$item['product_id']][0]['image'],
                'price' => $product['price'],
                'quantity' => $item['quantity']
            ];
        }

        $view = new ViewModel([
            'bagList' => $dataList
        ]);

        if($this->getRequest()->isPost())
        {
            $formData = $this->params()->fromPost();
            foreach ($formData['order'] as $key => $item)
            {
                $this->productRepository->bagUpate([
                    'datas' => [
                        'quantity' => $item['quantity']
                    ],
                    'id' => $key
                ]);
            }

            return $this->redirect()->toRoute('site/order');
        }

        $view->setTemplate('page/site/order-index');
        $this->layout()->setTemplate('layout/product_layout')->setVariables([
            'subTitle' => 'Sipariş <span>Listele</span>',
            'nav' => 'order'
        ]);

        if(count($dataList) > 0)
        {
            return $view;
        }else{
            return $this->redirect()->toRoute('site/product');
        }
    }

    public function viewAction()
    {
        $slug = $this->params()->fromRoute('slug');

        $view = new ViewModel();
        $view->setTemplate('page/site/order-view');
        $this->layout()->setTemplate('layout/product_layout')->setVariables([
            'subTitle' => 'Sipariş <span>İncele</span>',
            'nav' => 'order'
        ]);
        return $view;
    }

    public function removeAction()
    {
        header('Content-Type: application/json');
        $id = $this->params()->fromRoute('id');
        $result = $this->productRepository->removeBag($id);
        if($result)
        {
            print_r(json_encode([
                'code' => 200
            ]));
        }else{
            print_r(json_encode([
                'code' => 201
            ]));
        }
        die;
    }

    public function removeAllAction()
    {
        $this->productRepository->removeAllBag($_SESSION['userInfo']['id']);
        return $this->redirect()->toRoute('site/order');
    }

    public function paymentAction()
    {
        $bag = $this->productRepository->bagList($_SESSION['userInfo']['id']);
        $products = [];
        foreach ($bag as $item)
        {
            $products[$item['product_id']] = $item['quantity'];
        }

        $view = new ViewModel([
            'bag' => $bag,
            'productList' => $this->productRepository->productList()
        ]);

        if($this->getRequest()->isPost())
        {
            $formData = $this->params()->fromPost();
            $formData = array_merge($formData,[
                'user_id' => $_SESSION['userInfo']['id'],
                'products' => json_encode($products),
                'options' => json_encode([
                    'name' => $formData['name'],
                    'surname' => $formData['surname']
                ]),
                'create_date' => date('Y-m-d H:i:s'),
                'status' => 'P'
            ]);
            unset($formData['name']);
            unset($formData['surname']);
            $status = $this->orderRepository->pendingOrders($formData);
            if($status)
            {
                $this->productRepository->removeAllBag($_SESSION['userInfo']['id']);
                return $this->redirect()->toRoute('site/user/order');
            }
        }

        $view->setTemplate('page/site/order-payment');
        $this->layout()->setTemplate('layout/product_layout')->setVariables([
            'subTitle' => 'Ödeme <span>Islemleri</span>',
            'nav' => 'order'
        ]);
        return $view;
    }
}
