<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace User\Controller;

use Feedback\Repository\FeedbackCommentRespository;
use Feedback\Repository\FeedbackRepository;
use Order\Repository\OrderRepository;
use Product\Repository\ProductRepository;
use User\Form\FollowCreateForm;
use User\Repository\FollowRepository;
use User\Repository\UserRepository;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class SiteController extends AbstractActionController
{
    private $userRepository;
    private $followRepository;
    private $productRepository;
    private $feedbackRepository;
    private $feedbackCommentRepository;
    private $orderRepository;

    public function __construct()
    {
        $this->userRepository = new UserRepository();
        $this->followRepository = new FollowRepository();
        $this->productRepository = new ProductRepository();
        $this->feedbackRepository = new FeedbackRepository();
        $this->feedbackCommentRepository = new FeedbackCommentRespository();
        $this->orderRepository = new OrderRepository();
    }

    public function indexAction()
    {
        $view = new ViewModel();
        $view->setTemplate('page/site/user-index');
        $this->layout()->setTemplate('layout/user_site_layout')->setVariables([
            'subTitle' => 'Hesabım',
            'userPanel' => 'home'
        ]);

        if(isset($_SESSION['hobimaksSession']) == true)
        {
            $view->setVariables([
                'referenceList' => $this->userRepository->userReferenceList($_SESSION['userInfo']['id'])
            ]);
            return $view;
        }else{
            return $this->redirect()->toRoute('site/user/login');
        }
    }

    public function loginAction()
    {
        $view = new ViewModel();

        if($this->getRequest()->isPost())
        {
            $formData = $this->params()->fromPost();
            $loginControl = $this->userRepository->userFindByOne([
                'query' => sprintf('username="%s" AND password="%s" AND status="Y"',$formData['username'],hash('sha256',$formData['password']))
            ]);

            if(count($loginControl) > 0)
            {
                $_SESSION['hobimaksSession'] = true;
                $_SESSION['userInfo'] = [
                    'id' => $loginControl['id'],
                    'username' => $loginControl['username'],
                    'email' => $loginControl['email'],
                    'create_date' => $loginControl['create_date'],
                ];

                return $this->redirect()->toRoute('site/user');
            }else{
                $_SESSION['message'] = [
                    'code' => 201,
                    'message' => 'Kullanıcı bilgileriniz hatalı.Lütfen tekrar deneyiniz.'
                ];
            }
        }

        $view->setTemplate('page/site/user-login');
        $this->layout()->setTemplate('layout/user_layout')->setVariables([
            'subTitle' => 'Kullanici <span>Girisi</span>',
            'user' => 'login'
        ]);

        if(isset($_SESSION['hobimaksSession']) == false)
        {
            return $view;
        }else{
            return $this->redirect()->toRoute('site/user');
        }
    }

    public function registerAction()
    {
        $view = new ViewModel([
            'reference' => false
        ]);

        if($this->getRequest()->isPost())
        {
            $formData = $this->params()->fromPost();

            if(count($this->userRepository->userFindByOne([
                'query' => sprintf('username="%s"',$formData['username'])
            ])) > 0)
            {
                $_SESSION['message'] = [
                    'code' => 201,
                    'message' => 'Belirtilen kullanıcı adı sistemimizde kayıtlıdır.'
                ];
            }elseif (count($this->userRepository->userFindByOne([
                    'query' => sprintf('email="%s"',$formData['email'])
                ])) > 0)
            {
                $_SESSION['message'] = [
                    'code' => 201,
                    'message' => 'Belirtilen e-posta adresi sistemimizde kayıtlıdır.'
                ];
            }elseif ($formData['password'] != $formData['re_password'])
            {
                $_SESSION['message'] = [
                    'code' => 201,
                    'message' => 'Parolalar uyuşmuyor.'
                ];
            }else{
                $formData = array_merge($formData,['parent_id' => 0,'permission' => 1]);
                $result = $this->userRepository->userSave($formData);
                if($result)
                {
                    $status = $this->userRepository->userInfoSave([
                        'user_id' => $result
                    ]);
                    if($status)
                    {
                        unset($_SESSION['message']);
                        return $this->redirect()->toRoute('site/user/login');
                    }else{
                        $_SESSION['message'] = [
                            'code' => 201,
                            'message' => 'Kayıt sırasında beklenmedik bir hata oluştu.'
                        ];
                    }
                }else{
                    $_SESSION['message'] = [
                        'code' => 201,
                        'message' => 'Kayıt sırasında beklenmedik bir hata oluştu.'
                    ];
                }
            }
            die;
        }
        $view->setTemplate('page/site/user-register');
        $this->layout()->setTemplate('layout/user_layout')->setVariables([
            'subTitle' => 'Kullanici <span>Kayit</span>',
            'user' => 'register'
        ]);

        if(isset($_SESSION['hobimaksSession']) == false)
        {
            return $view;
        }else{
            return $this->redirect()->toRoute('site/user');
        }
    }

    public function referenceAction()
    {
        $code = $this->params()->fromRoute('code');
        $referenceName = $this->userRepository->userFindByOne([
            'query' => sprintf('username="%s"',$code)
        ]);

        $view = new ViewModel([
            'reference' => true,
            'referenceName' => $referenceName['username']
        ]);

        if($this->getRequest()->isPost())
        {
            $formData = $this->params()->fromPost();

            if(count($this->userRepository->userFindByOne([
                'query' => sprintf('username="%s"',$formData['username'])
            ])) > 0)
            {
                $_SESSION['message'] = [
                    'code' => 201,
                    'message' => 'Belirtilen kullanıcı adı sistemimizde kayıtlıdır.'
                ];
            }elseif (count($this->userRepository->userFindByOne([
                    'query' => sprintf('email="%s"',$formData['email'])
                ])) > 0)
            {
                $_SESSION['message'] = [
                    'code' => 201,
                    'message' => 'Belirtilen e-posta adresi sistemimizde kayıtlıdır.'
                ];
            }elseif ($formData['password'] != $formData['re_password'])
            {
                $_SESSION['message'] = [
                    'code' => 201,
                    'message' => 'Parolalar uyuşmuyor.'
                ];
            }else{
                $formData = array_merge($formData,['parent_id' => $referenceName['id'],'permission' => 1]);

                $result = $this->userRepository->userSave($formData);
                if($result)
                {
                    $status = $this->userRepository->userInfoSave([
                        'user_id' => $result
                    ]);
                    if($status)
                    {
                        unset($_SESSION['message']);
                        return $this->redirect()->toRoute('site/user/login');
                    }else{
                        $_SESSION['message'] = [
                            'code' => 201,
                            'message' => 'Kayıt sırasında beklenmedik bir hata oluştu.'
                        ];
                    }
                }else{
                    $_SESSION['message'] = [
                        'code' => 201,
                        'message' => 'Kayıt sırasında beklenmedik bir hata oluştu.'
                    ];
                }
            }
        }
        $view->setTemplate('page/site/user-register');
        $this->layout()->setTemplate('layout/user_layout')->setVariables([
            'subTitle' => 'Kullanici <span>Kayit</span>',
            'user' => 'register'
        ]);

        if(isset($_SESSION['hobimaksSession']) == false)
        {
            if(count($referenceName) > 0)
            {
                return $view;
            }else{
                return $this->redirect()->toRoute('site/user/register');
            }
        }else{
            return $this->redirect()->toRoute('site/user');
        }
    }

    public function logoutAction()
    {
        if(isset($_SESSION['hobimaksSession']) == true)
        {
            $_SESSION['hobimaksSession'] = false;
            session_destroy();

            return $this->redirect()->toRoute('site/user/login');
        }else{
            return $this->redirect()->toRoute('site/user/login');
        }
    }

    public function orderAction()
    {
        $orderList = [];
        foreach ($this->orderRepository->pendingOrdersList($_SESSION['userInfo']['id']) as $item)
        {
            $status = null;
            switch ($item['status']) {
                case 'P':
                    $status = '<label class="highlight buttercup-bg">Beklemede</label>';
                    break;
                case 'Y':
                    $status = '<label class="highlight meadow-bg">Onaylandı</label>';
                    break;
                case 'N':
                    $status = '<label class="highlight mandy-bg">İptal Edildi</label>';
            }
            $orderList[] = [
                'id' => $item['id'],
                'create_date' => $item['create_date'],
                'status' => $status,
                'order_code' => $item['order_code']
            ];
        }

        $view = new ViewModel([
            'orderList' => $orderList
        ]);
        $view->setTemplate('page/site/order-list');
        $this->layout()->setTemplate('layout/user_site_layout')->setVariables([
            'subTitle' => 'Sipariş <span>Listesi</span>',
            'user' => 'home',
            'userPanel' => 'product'
        ]);

        if(isset($_SESSION['hobimaksSession']) == true)
        {
            return $view;
        }else{
            return $this->redirect()->toRoute('site/user');
        }
    }

    public function settingsAction()
    {
        $view = new ViewModel([
            'user_core' => $this->userRepository->userFindByOne([
                'query' => sprintf('id=%d',$_SESSION['userInfo']['id'])
            ]),
            'user_info' => $this->userRepository->userInfoFindByOne(sprintf('user_id=%d',$_SESSION['userInfo']['id'])),
            'dietList' => $this->userRepository->dietList()
        ]);
        $view->setTemplate('page/site/user-settings');
        $this->layout()->setTemplate('layout/user_site_layout')->setVariables([
            'subTitle' => 'Hesap <span>Ayarları</span>',
            'user' => 'home',
            'userPanel' => 'settings'
        ]);

        if($this->getRequest()->isPost())
        {
            $formData = $this->params()->fromPost();

            if(array_key_exists('info',$formData))
            {
                unset($formData['info']);
                $result = $this->userRepository->userInfoUpdate([
                    'datas' => [
                        'name' => $formData['name'],
                        'surname' => $formData['surname'],
                        'gsm' => $formData['gsm'],
                        'diet_id' => $formData['diet'] == 0 ? null : $formData['diet']
                    ],
                    'user_id' => $_SESSION['userInfo']['id']
                ]);

                if($result)
                {
                    $_SESSION['message'] = [
                        'code' => 200,
                        'message' => 'İşleminiz başarılı bir şekilde gerçekleştirilmiştir.'
                    ];

                    return $this->redirect()->toRoute('site/user');
                }else{
                    $_SESSION['message'] = [
                        'code' => 201,
                        'message' => 'Bir hata ile karşılaştık.Lütfen daha sonra tekrar deneyiniz.'
                    ];
                }
            }elseif (array_key_exists('password_post',$formData))
            {
                unset($formData['password_post']);

                if(count($this->userRepository->userFindByOne([
                    'query' => sprintf('username="%s" AND password="%s"',$_SESSION['userInfo']['username'],hash('sha256',$formData['password']))
                ])) > 0)
                {
                    if($formData['password'] == $formData['new_password'])
                    {
                        $_SESSION['message'] = [
                            'code' => 201,
                            'message' => 'Yeni parolanız mevcut parolanız ile aynı olmamalıdır.'
                        ];
                    }elseif($formData['new_password'] != $formData['re_password']){
                        $_SESSION['message'] = [
                            'code' => 201,
                            'message' => 'Yeni parolalarınız bir biri ile eşleşmiyor.'
                        ];
                    }else{
                        $result = $this->userRepository->userUpdate([
                            'datas' => [
                                'password' => hash('sha256',$formData['new_password'])
                            ],
                            'id' => $_SESSION['userInfo']['id']
                        ]);

                        if($result)
                        {
                            $_SESSION['message'] = [
                                'code' => 200,
                                'message' => 'İşleminiz başarılı bir şekilde gerçekleştirilmiştir.'
                            ];

                            return $this->redirect()->toRoute('site/user');
                        }else{
                            $_SESSION['message'] = [
                                'code' => 201,
                                'message' => 'Bir hata ile karşılaştık.Lütfen daha sonra tekrar deneyiniz.'
                            ];
                        }
                    }
                }else{
                    $_SESSION['message'] = [
                        'code' => 201,
                        'message' => 'Lütfen mevcut parolanızı doğru giriniz.'
                    ];
                }
            }
        }

        if(isset($_SESSION['hobimaksSession']) == true)
        {
            return $view;
        }else{
            return $this->redirect()->toRoute('site/user');
        }
    }

    public function manuelAction()
    {
        $manuelList = [];

        foreach ($this->userRepository->userList() as $item) {
            if($item['parent_id'] == $_SESSION['userInfo']['id'])
            {
                $info = $this->userRepository->userInfoFindByOne(sprintf('user_id=%d',$item['id']));
                $day = date('Y') - $item['birth_day'];
                $manuelList[] = [
                    'user_id' => $item['id'],
                    'user' => $info['name'].' '.$info['surname'].' ('.$day.')',
                    'create_date' => $item['create_date']
                ];
            }elseif ($item['id'] == $_SESSION['userInfo']['id'])
            {
                $manuelList[] = [
                    'user_id' => $item['id'],
                    'user' => 'Siz',
                    'create_date' => $item['create_date']
                ];
            }
        }

        $view = new ViewModel([
            'manuelList' => $manuelList
        ]);
        $view->setTemplate('page/site/manuel/manuel-list');
        $this->layout()->setTemplate('layout/user_site_layout')->setVariables([
            'subTitle' => 'Manuel <span>Listesi</span>',
            'user' => 'home',
            'userPanel' => 'manuel'
        ]);

        if(isset($_SESSION['hobimaksSession']) == true)
        {
            return $view;
        }else{
            return $this->redirect()->toRoute('site/user');
        }
    }

    public function createAction()
    {
        $form = new FollowCreateForm();
        $childUsers = $this->userRepository->userFindByOne([
            'query' => sprintf('parent_id=%d',$_SESSION['userInfo']['id'])
        ]);

        $view = new ViewModel([
            'form' => $form,
            'childUsers' => $childUsers
        ]);

        if($this->getRequest()->isPost())
        {
            $formData = $this->params()->fromPost();
            $formData = array_merge($formData,['admin_id' => $_SESSION['userInfo']['id']]);

            $result = $this->followRepository->followSave([
                'admin_id' => $_SESSION['userInfo']['id'],
                'user_id' => $formData['user_id'],
                'actions' => json_encode($formData['follow']),
                'create_date' => date('Y-m-d H:i:s')
            ]);

            if($result)
            {
                return $this->redirect()->toRoute('site/user/manuel');
            }else{
                $_SESSION['message'] = [
                    'code' => 201,
                    'message' => 'İşleminiz sırasında bir hata ile karşılaştık.Lütfen daha sonra tekrar deneyiniz.'
                ];
            }
        }

        $view->setTemplate('page/site/manuel/manuel-create');
        $this->layout()->setTemplate('layout/user_site_layout')->setVariables([
            'subTitle' => 'Takip <span>Oluştur</span>',
            'user' => 'home',
            'userPanel' => 'manuel'
        ]);

        if(isset($_SESSION['hobimaksSession']) == true)
        {
            return $view;
        }else{
            return $this->redirect()->toRoute('site/user');
        }
    }

    public function viewAction()
    {
        $dataList =  [];
        $id = $this->params()->fromRoute('id');
        foreach ($this->followRepository->followList() as $item)
        {
            if($item['user_id'] == $id)
            {
                $dataList[] = $item;
            }
        }

        $info = $this->userRepository->userInfoFindByOne(sprintf('user_id=%d',$id));
        $view = new ViewModel([
            'id' => $id,
            'dataList' => $dataList
        ]);
        $view->setTemplate('page/site/manuel/manuel-view');
        $this->layout()->setTemplate('layout/user_site_layout')->setVariables([
            'subTitle' => $info['name'].' '.$info['surname'].' <span> Takip</span>',
            'user' => 'home',
            'userPanel' => 'manuel'
        ]);

        if(isset($_SESSION['hobimaksSession']) == true)
        {
            return $view;
        }else{
            return $this->redirect()->toRoute('site/user');
        }
    }

    public function xhrAction()
    {
        header('Content-Type: application/json');
        $id = $this->params()->fromRoute('id');
        $dataList = [];

        if(isset($_SESSION['hobimaksSession']) == true)
        {
            $i=1;
            foreach ($this->followRepository->followList() as $item)
            {
                if($item['user_id'] == $id)
                {
                    $actions = json_decode($item['actions']);
                    $dataList['aaData'][] = [
                        $i++.'. Hafta',
                        $item['create_date'],
                        $actions->kilo,
                        $actions->boy,
                        $actions->gogus,
                        $actions->kol,
                        $actions->gobek,
                        $actions->basen
                    ];
                }
            }
            print_r(json_encode($dataList));
            die;
        }else{
            return $this->redirect()->toRoute('site/user');
        }
    }

    public function dietAction()
    {
        $view = new ViewModel([
            'list' => json_decode($this->userRepository->dietList()[$this->userRepository->userInfoFindByOne(sprintf('user_id=%d',$_SESSION['userInfo']['id']))['diet_id']]['list'])
        ]);
        $view->setTemplate('page/site/user-diet');
        $this->layout()->setTemplate('layout/user_site_layout')->setVariables([
            'subTitle' => 'Diyet <span>Listesi</span>',
            'user' => 'home',
            'userPanel' => 'diet'
        ]);

        if(isset($_SESSION['hobimaksSession']) == true)
        {
            return $view;
        }else{
            return $this->redirect()->toRoute('site/user');
        }
    }

    public function userInfoAction()
    {
        $id = $this->params()->fromRoute('id');

        $userInfo = $this->userRepository->userFindByOne([
            'query' => sprintf('id=%d',$id)
        ]);
        $userInfo = array_merge($userInfo,['info' => $this->userRepository->userInfoFindByOne(sprintf('user_id=%d',$id))]);

        $view = new ViewModel([
            'userInfo' => $userInfo,
            'dietList' => $this->userRepository->dietList()
        ]);
        $view->setTemplate('page/site/user-info');
        $this->layout()->setTemplate('layout/user_site_layout')->setVariables([
            'subTitle' => 'Kullanıcı <span>Detayi</span>',
            'user' => 'home',
            'userPanel' => 'diet'
        ]);

        if(isset($_SESSION['hobimaksSession']) == true)
        {
            return $view;
        }else{
            return $this->redirect()->toRoute('site/user');
        }
    }

    public function feedbackAction()
    {
        $view = new ViewModel([
            'feedbackList' => $this->feedbackRepository->feedbackList(),
            'feedbackCommentsList' => $this->feedbackCommentRepository->feedbackCommentList()
        ]);
        $view->setTemplate('page/site/feedback-list');
        $this->layout()->setTemplate('layout/user_site_layout')->setVariables([
            'subTitle' => 'Feedback',
            'user' => 'feedback',
            'userPanel' => 'feedback'
        ]);

        if(isset($_SESSION['hobimaksSession']) == true)
        {
            return $view;
        }else{
            return $this->redirect()->toRoute('site/user');
        }
    }
}
