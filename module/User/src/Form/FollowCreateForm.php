<?php
/**
 * Created by PhpStorm.
 * User: Mehmet HAKKIOĞLU
 * Mail: contact@mehmethakkioglu.com
 * Date: 02.12.2018
 * Time: 13:00
 */
namespace User\Form;

use User\Repository\FollowRepository;
use User\Repository\UserRepository;
use Zend\Form\Element\Select;
use Zend\Form\Element\Text;
use Zend\Form\Form;

class FollowCreateForm extends Form
{
    public function __construct(string $name = null, array $config = [])
    {
        parent::__construct($name,$config);

        $this
            ->add([
                'type' => Select::class,
                'name' => 'user_id',
                'options' => [
                    'label' => 'Kullanıcı',
                    'value_options' => $this->userList()
                ],
                'attributes' => [
                    'class' => 'select2',
                    'data-jcf' => '{"wrapNative": false, "wrapNativeOnMobile": false}'
                ]
            ])
            ->add([
                'type' => Text::class,
                'name' => 'follow[kilo]',
                'options' => [
                    'label' => 'Kilo'
                ],
                'attributes' => [
                    'class' => 'form-control',
                    'data-type' => 'field'
                ]
            ])
            ->add([
                'type' => Text::class,
                'name' => 'follow[boy]',
                'options' => [
                    'label' => 'Boy'
                ],
                'attributes' => [
                    'class' => 'form-control',
                    'data-type' => 'field'
                ]
            ])
            ->add([
                'type' => Text::class,
                'name' => 'follow[gogus]',
                'options' => [
                    'label' => 'Göğüs'
                ],
                'attributes' => [
                    'class' => 'form-control',
                    'data-type' => 'field'
                ]
            ])
            ->add([
                'type' => Text::class,
                'name' => 'follow[kol]',
                'options' => [
                    'label' => 'Kol'
                ],
                'attributes' => [
                    'class' => 'form-control',
                    'data-type' => 'field'
                ]
            ])
            ->add([
                'type' => Text::class,
                'name' => 'follow[gobek]',
                'options' => [
                    'label' => 'Göbek'
                ],
                'attributes' => [
                    'class' => 'form-control',
                    'data-type' => 'field'
                ]
            ])
            ->add([
                'type' => Text::class,
                'name' => 'follow[basen]',
                'options' => [
                    'label' => 'Basen'
                ],
                'attributes' => [
                    'class' => 'form-control',
                    'data-type' => 'field'
                ]
            ]);
    }

    private function userList()
    {
        $userRepository = new UserRepository();
        $dataList = [];
        foreach ($userRepository->userList() as $key => $item) {
            if($item['parent_id'] == $_SESSION['userInfo']['id'])
            {
                $info = $userRepository->userInfoFindByOne(sprintf('user_id=%d',$item['id']));
                $birth = date('Y') - $item['birth_day'];
                $dataList[$item['id']] = $info['name'].' '.$info['surname'].' ('.$birth.')';
            }elseif ($item['id'] == $_SESSION['userInfo']['id'])
            {
                $dataList[$item['id']] = 'Kendiniz';
            }
        }

        return $dataList;
    }
}