<?php
/**
 * Created by PhpStorm.
 * User: Mehmet HAKKIOĞLU
 * Mail: contact@mehmethakkioglu.com
 * Date: 16.11.2018
 * Time: 02:57
 */
namespace User\Repository;

use Core\Classes\MethaDatabase;
use Zend\Db\TableGateway\TableGateway;

class UserRepository
{
    private $adapter;

    public function __construct()
    {
        $adapter = new MethaDatabase();
        $this->adapter = $adapter->MethaDatabaseConnection();
    }

    public function userFindByOne(array $data)
    {
        $dataList = [];
        $table = new TableGateway('user_core',$this->adapter);
        $rows = $table->select($data['query']);

        foreach ($rows as $item)
        {
            $dataList = [
                'id' => $item['id'],
                'username' => $item['username'],
                'password' => $item['password'],
                'email' => $item['email'],
                'create_date' => $item['create_date'],
                'birth_day' => $item['birth_day'],
                'status' => $item['status']
            ];
        }

        return $dataList;
    }

    public function userList()
    {
        $dataList = [];
        $table = new TableGateway('user_core',$this->adapter);
        $rows = $table->select();

        foreach ($rows as $item)
        {
            $dataList[] = [
                'id' => $item['id'],
                'username' => $item['username'],
                'password' => $item['password'],
                'email' => $item['email'],
                'create_date' => $item['create_date'],
                'parent_id' => $item['parent_id'],
                'birth_day' => $item['birth_day'],
                'status' => $item['status']
            ];
        }

        return $dataList;
    }

    public function userSave(array $data)
    {
        $table = new TableGateway('user_core',$this->adapter);
        $result = $table->insert([
            'username' => $data['username'],
            'password' => hash('sha256',$data['password']),
            'email' => $data['email'],
            'parent_id' => $data['parent_id'],
            'permission' => $data['permission'],
            'birth_day' => $data['birth_day'],
            'create_date' => date('Y-m-d H:i:s'),
            'status' => 'Y'
        ]);

        if($result)
        {
            return $this->adapter->getDriver()->getLastGeneratedValue();
        }else{
            return false;
        }
    }

    public function userReferenceList(int $id)
    {
        $dataList = [];
        $table = new TableGateway('user_core',$this->adapter);
        $rows = $table->select(sprintf('parent_id=%d',$id));

        foreach ($rows as $item)
        {
            $dataList[] = [
                'id' => $item['id'],
                'username' => $item['username'],
                'create_date' => $item['create_date']
            ];
        }

        return $dataList;
    }

    public function userUpdate(array $data)
    {
        $table = new TableGateway('user_core',$this->adapter);
        return $table->update($data['datas'],[
            'id' => $data['id']
        ]);
    }

    public function userInfoUpdate(array $data)
    {
        $table = new TableGateway('user_info',$this->adapter);
        return $table->update($data['datas'],[
            'user_id' => $data['user_id']
        ]);
    }

    public function userInfoSave(array $data)
    {
        $table = new TableGateway('user_info',$this->adapter);
        return $table->insert($data);
    }

    public function userInfoFindByOne(string $query)
    {
        $dataList = [];
        $table = new TableGateway('user_info',$this->adapter);
        $rows = $table->select($query);

        foreach ($rows as $item)
        {
            $dataList = [
                'user_id' => $item['user_id'],
                'name' => $item['name'],
                'surname' => $item['surname'],
                'birth_day' => $item['birth_day'],
                'diet_id' => $item['diet_id'],
                'gsm' => $item['gsm']
            ];
        }

        return $dataList;
    }

    public function userInfoList()
    {
        $dataList = [];
        $table = new TableGateway('user_info',$this->adapter);
        $rows = $table->select();

        foreach ($rows as $item)
        {
            $dataList[$item['user_id']] = [
                'user_id' => $item['user_id'],
                'name' => $item['name'],
                'surname' => $item['surname'],
                'birth_day' => $item['birth_day'],
                'diet_id' => $item['diet_id'],
                'gsm' => $item['gsm']
            ];
        }

        return $dataList;
    }

    public function dietList()
    {
        $dataList = [];
        $table = new TableGateway('diet_core',$this->adapter);
        $rows = $table->select();

        foreach ($rows as $item)
        {
            if($item['status'] == 'Y')
            {
                $dataList[$item['id']] = [
                    'id' => $item['id'],
                    'title' => $item['title'],
                    'list' => $item['list']
                ];
            }
        }

        return $dataList;
    }
}