<?php
/**
 * Created by PhpStorm.
 * User: Mehmet HAKKIOĞLU
 * Mail: contact@mehmethakkioglu.com
 * Date: 02.12.2018
 * Time: 12:54
 */
namespace User\Repository;

use Core\Classes\MethaDatabase;
use Zend\Db\TableGateway\TableGateway;

class FollowRepository
{
    private $adapter;

    public function __construct()
    {
        $adapter = new MethaDatabase();
        $this->adapter = $adapter->MethaDatabaseConnection();
    }

    public function followSave(array $data)
    {
        $table = new TableGateway('follow_core',$this->adapter);
        return $table->insert($data);
    }

    public function followList()
    {
        $dataList = [];
        $table = new TableGateway('follow_core',$this->adapter);
        $rows = $table->select();

        foreach ($rows as $item)
        {
            $dataList[] = [
                'id' => $item['id'],
                'admin_id' => $item['admin_id'],
                'user_id' => $item['user_id'],
                'actions' => $item['actions'],
                'create_date' => $item['create_date']
            ];
        }

        return $dataList;
    }

    public function followFindByOneList(string $query)
    {
        $dataList = [];
        $table = new TableGateway('follow_core',$this->adapter);
        $rows = $table->select($query);

        foreach ($rows as $item)
        {
            $dataList[] = [
                'id' => $item['id'],
                'admin_id' => $item['admin_id'],
                'user_id' => $item['user_id'],
                'actions' => $item['actions'],
                'create_date' => $item['create_date']
            ];
        }

        return $dataList;
    }

    public function followRemove(int $id)
    {
        $table = new TableGateway('follow_core',$this->adapter);
        return $table->delete([
            'id' => $id
        ]);
    }

    public function followUpdate(array $data)
    {
        $table = new TableGateway('follow_core',$this->adapter);
        return $table->update($data['datas'],[
            'id' => $data['id']
        ]);
    }
}