<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace User;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'router' => [
        'routes' => [
            'site' => [
                'child_routes' => [
                    'user' => [
                        'type' => Segment::class,
                        'may_terminate' => true,
                        'options' => [
                            'route'    => 'user[/]',
                            'defaults' => [
                                'controller' => Controller\SiteController::class,
                                'action'     => 'index',
                            ],
                        ],
                        'child_routes' => [
                            'userInfo' => [
                                'type' => Segment::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => 'userInfo/:id[/]',
                                    'defaults' => [
                                        'controller' => Controller\SiteController::class,
                                        'action'     => 'userInfo',
                                    ],
                                ],
                            ],
                            'login' => [
                                'type' => Segment::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => 'login[/]',
                                    'defaults' => [
                                        'controller' => Controller\SiteController::class,
                                        'action'     => 'login',
                                    ],
                                ],
                            ],
                            'register' => [
                                'type' => Segment::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => 'register[/]',
                                    'defaults' => [
                                        'controller' => Controller\SiteController::class,
                                        'action'     => 'register',
                                    ],
                                ],
                            ],
                            'logout' => [
                                'type' => Segment::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => 'logout[/]',
                                    'defaults' => [
                                        'controller' => Controller\SiteController::class,
                                        'action'     => 'logout',
                                    ],
                                ],
                            ],
                            'reference' => [
                                'type' => Segment::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => 'reference/:code[/]',
                                    'defaults' => [
                                        'controller' => Controller\SiteController::class,
                                        'action'     => 'reference',
                                    ],
                                ],
                            ],
                            'order' => [
                                'type' => Segment::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => 'order[/]',
                                    'defaults' => [
                                        'controller' => Controller\SiteController::class,
                                        'action'     => 'order',
                                    ],
                                ],
                            ],
                            'settings' => [
                                'type' => Segment::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => 'settings[/]',
                                    'defaults' => [
                                        'controller' => Controller\SiteController::class,
                                        'action'     => 'settings',
                                    ],
                                ],
                            ],
                            'diet' => [
                                'type' => Segment::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => 'diet[/]',
                                    'defaults' => [
                                        'controller' => Controller\SiteController::class,
                                        'action'     => 'diet',
                                    ],
                                ],
                            ],
                            'feedback' => [
                                'type' => Segment::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => 'feedback[/]',
                                    'defaults' => [
                                        'controller' => Controller\SiteController::class,
                                        'action'     => 'feedback',
                                    ],
                                ],
                            ],
                            'manuel' => [
                                'type' => Segment::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => 'manuel[/]',
                                    'defaults' => [
                                        'controller' => Controller\SiteController::class,
                                        'action'     => 'manuel',
                                    ],
                                ],
                                'child_routes' => [
                                    'create' => [
                                        'type' => Segment::class,
                                        'may_terminate' => true,
                                        'options' => [
                                            'route'    => 'create[/]',
                                            'defaults' => [
                                                'controller' => Controller\SiteController::class,
                                                'action'     => 'create',
                                            ],
                                        ],
                                    ],
                                    'view' => [
                                        'type' => Segment::class,
                                        'may_terminate' => true,
                                        'options' => [
                                            'route'    => 'view/:id[/]',
                                            'defaults' => [
                                                'controller' => Controller\SiteController::class,
                                                'action'     => 'view',
                                            ],
                                        ],
                                    ],
                                    'xhr' => [
                                        'type' => Segment::class,
                                        'may_terminate' => true,
                                        'options' => [
                                            'route'    => 'xhr/:id[/]',
                                            'defaults' => [
                                                'controller' => Controller\SiteController::class,
                                                'action'     => 'xhr',
                                            ],
                                        ],
                                    ],
                                ]
                            ],
                        ]
                    ],
                ]
            ],
            'panel' => [
                'child_routes' => [
                    'user' => [
                        'type'    => Segment::class,
                        'options' => [
                            'route'    => 'user[/]',
                            'defaults' => [
                                'controller' => Controller\PanelController::class,
                                'action'     => 'index',
                            ],
                        ],
                    ],
                ]
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\PanelController::class => InvokableFactory::class,
            Controller\SiteController::class => InvokableFactory::class,
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
];
