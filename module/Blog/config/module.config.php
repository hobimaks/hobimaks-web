<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Blog;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'router' => [
        'routes' => [
            'site' => [
                'child_routes' => [
                    'blog' => [
                        'type' => Segment::class,
                        'may_terminate' => true,
                        'options' => [
                            'route'    => 'blog[/]',
                            'defaults' => [
                                'controller' => Controller\SiteController::class,
                                'action'     => 'blog',
                            ],
                        ],
                        'child_routes' => [
                            'category' => [
                                'type' => Segment::class,
                                'may_terminate' => true,
                                'options' => [
                                    'route'    => ':category[/]',
                                    'defaults' => [
                                        'controller' => Controller\SiteController::class,
                                        'action'     => 'category',
                                    ],
                                ],
                                'child_routes' => [
                                    'view' => [
                                        'type' => Segment::class,
                                        'may_terminate' => true,
                                        'options' => [
                                            'route'    => ':view[/]',
                                            'defaults' => [
                                                'controller' => Controller\SiteController::class,
                                                'action'     => 'view',
                                            ],
                                        ],
                                    ],
                                ]
                            ],
                        ]
                    ],
                ]
            ],
            'panel' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/panel[/]',
                    'defaults' => [
                        'controller' => Controller\PanelController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\PanelController::class => InvokableFactory::class,
            Controller\SiteController::class => InvokableFactory::class,
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
];
