<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Blog\Controller;

use Blog\Repository\BlogRepository;
use Blog\Repository\BlogImageRepository;
use Product\Repository\ProductRepository;
use Product\Repository\ProductImageRepository;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class SiteController extends AbstractActionController
{
    private $blogRepository;
    private $blogImageRepository;
    private $productRepository;
    private $productImageRepository;

    public function __construct()
    {
        $this->blogRepository = new BlogRepository();
        $this->blogImageRepository = new BlogImageRepository();
        $this->productRepository = new ProductRepository();
        $this->productImageRepository = new ProductImageRepository();
    }

    public function blogAction()
    {
        $view = new ViewModel([
            'blogList' => $this->blogRepository->blogList(),
            'blogImageList' => $this->blogImageRepository->blogImageList()
        ]);
        $view->setTemplate('page/site/blog-index');
        $this->layout()->setTemplate('layout/blog_layout')->setVariables([
            'nav' => 'blog',
            'productList' => $this->productRepository->productList()
        ]);
        return $view;
    }

    public function categoryAction()
    {
        $dataList = [];
        $category = $this->params()->fromRoute('category');
        foreach($this->blogRepository->blogList() as $item)
        {
            if(json_decode($item['options'])->category == $category && $item['status'] == 'Y')
            {
                $dataList[] = [
                    'id' => $item['id'],
                    'title' => $item['title'],
                    'content' => $item['content'],
                    'create_date' => $item['create_date'],
                    'options' => $item['options'],
                    'slug' => $item['slug']
                ];
            }
        }

        $view = new ViewModel([
            'blogList' => $dataList,
            'blogImageList' => $this->blogImageRepository->blogImageList()
        ]);
        $view->setTemplate('page/site/blog-category');
        $this->layout()->setTemplate('layout/blog_layout')->setVariables([
            'nav' => 'blog',
            'productList' => $this->productRepository->productList()
        ]);
        return $view;
    }

    public function viewAction()
    {
        $blog = $this->params()->fromRoute('view');
        $view = new ViewModel([
            'blogList' => $this->blogRepository->blogFindByOneList(sprintf('slug="%s"',$blog)),
            'blogImageList' => $this->blogImageRepository->blogImageList()
        ]);
        $view->setTemplate('page/site/blog-view');
        $this->layout()->setTemplate('layout/blog_layout')->setVariables([
            'nav' => 'blog',
            'productList' => $this->productRepository->productList(),
            'type' => 'view'
        ]);
        return $view;
    }
}
