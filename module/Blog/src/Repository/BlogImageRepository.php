<?php
/**
 * Created by PhpStorm.
 * User: Mehmet HAKKIOĞLU
 * Mail: contact@mehmethakkioglu.com
 * Date: 02.12.2018
 * Time: 16:27
 */
namespace Blog\Repository;

use Core\Classes\MethaDatabase;
use Zend\Db\TableGateway\TableGateway;

class BlogImageRepository
{
    private $adapter;

    public function __construct()
    {
        $adapter = new MethaDatabase();
        $this->adapter = $adapter->MethaDatabaseConnection();
    }

    public function blogImageSave(array $data)
    {
        $table = new TableGateway('blog_images',$this->adapter);
        return $table->insert($data);
    }

    public function blogImageUpdate(array $data)
    {
        $table = new TableGateway('blog_images',$this->adapter);
        return $table->update($data['datas'],['id' => $data['id']]);
    }

    public function blogImageRemove(int $id)
    {
        $table = new TableGateway('blog_images',$this->adapter);
        return $table->delete(['id' => $id]);
    }

    public function blogImageList()
    {
        $dataList = [];
        $table = new TableGateway('blog_images',$this->adapter);
        $rows = $table->select();

        foreach ($rows as $item)
        {
            $dataList[$item['blog_id']][] = [
                'id' => $item['id'],
                'blog_id' => $item['blog_id'],
                'name' => $item['name'],
                'type' => $item['type']
            ];
        }

        return $dataList;
    }

    public function blogImageFindByOneList(string $query)
    {
        $dataList = [];
        $table = new TableGateway('blog_images',$this->adapter);
        $rows = $table->select($query);

        foreach ($rows as $item)
        {
            $dataList = [
                'id' => $item['id'],
                'blog_id' => $item['blog_id'],
                'name' => $item['name'],
                'type' => $item['type']
            ];
        }

        return $dataList;
    }
}