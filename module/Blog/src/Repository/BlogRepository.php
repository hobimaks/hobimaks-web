<?php
/**
 * Created by PhpStorm.
 * User: Mehmet HAKKIOĞLU
 * Mail: contact@mehmethakkioglu.com
 * Date: 02.12.2018
 * Time: 16:20
 */
namespace Blog\Repository;

use Core\Classes\MethaDatabase;
use Zend\Db\TableGateway\TableGateway;

class BlogRepository
{
    private $adapter;

    public function __construct()
    {
        $adapter = new MethaDatabase();
        $this->adapter = $adapter->MethaDatabaseConnection();
    }

    public function blogSave(array $data)
    {
        $table = new TableGateway('blog_core',$this->adapter);
        return $table->insert($data);
    }

    public function blogUpdate(array $data)
    {
        $table = new TableGateway('blog_core',$this->adapter);
        return $table->update($data['datas'],['id' => $data['id']]);
    }

    public function blogRemove(int $id)
    {
        $table = new TableGateway('blog_core',$this->adapter);
        return $table->delete(['id' => $id]);
    }

    public function blogList()
    {
        $dataList = [];
        $table = new TableGateway('blog_core',$this->adapter);
        $rows = $table->select();

        foreach ($rows as $item)
        {
            $dataList[] = [
                'id' => $item['id'],
                'title' => $item['title'],
                'content' => $item['content'],
                'create_date' => $item['create_date'],
                'options' => $item['options'],
                'slug' => $item['slug'],
                'status' => $item['status']
            ];
        }

        return $dataList;
    }

    public function blogFindByOneList(string $query)
    {
        $dataList = [];
        $table = new TableGateway('blog_core',$this->adapter);
        $rows = $table->select($query);

        foreach ($rows as $item)
        {
            $dataList = [
                'id' => $item['id'],
                'title' => $item['title'],
                'content' => $item['content'],
                'create_date' => $item['create_date'],
                'options' => $item['options'],
                'slug' => $item['slug'],
                'status' => $item['status']
            ];
        }

        return $dataList;
    }
}